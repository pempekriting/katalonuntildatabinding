<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>DevExpress Regression</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>1e7f8d01-2498-4627-93a7-b4e1b4319b09</testSuiteGuid>
   <testCaseLink>
      <guid>e84f7133-affe-4587-b8d3-4ad705e944c4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Checklist all options</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>daad3a15-dd95-4621-80cc-a6ab29a7f83e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <iterationNameVariable>
         <defaultValue>''</defaultValue>
         <description></description>
         <id>92f3ffdf-0d16-495b-9d21-a43ecb334a5b</id>
         <masked>false</masked>
         <name>commentsValue</name>
      </iterationNameVariable>
      <testCaseId>Test Cases/Submit comment with data binding</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>3c254016-8721-4fbe-948d-7577c716b8c2</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/commentResource</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>3c254016-8721-4fbe-948d-7577c716b8c2</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Comments</value>
         <variableId>92f3ffdf-0d16-495b-9d21-a43ecb334a5b</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
